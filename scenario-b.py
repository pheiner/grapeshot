#!/usr/bin/env python3
import fileinput
import time
import datetime
import requests

reqs    = 0
total   = 0.0
avg     = 0.0
longest = 0.0

for line in fileinput.input():
    try:
        req = requests.request(url=line,method='GET')
        req_time = req.elapsed.total_seconds()
        print("request to %s took %s seconds" % (line, req_time))
        total += req_time 
        reqs += 1
        avg = total/reqs
        if req_time > longest:
            longest = req_time
    except:
        print("request failed")
    time.sleep(1)
    
print("Requests: %d, avg.time: %f s, longest req: %f s" % (reqs, avg, longest))
