#!/usr/bin/perl -w

while (<>) {
    chomp;
    my ($resptime) = /^\S+ \S+ \S+ \[.*?\] \".*?\" \S+ \S+ \".*?\" \".*?\" (\S+) .*/o;
    print $_ . "\n" if $resptime > 1000000;
}
