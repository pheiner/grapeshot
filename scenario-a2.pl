#!/usr/bin/perl -w

use Data::Dumper;

my %items;
my %results;

sub average {
        my (@values) = @_;

        my $count = scalar @values;
        my $total = 0; 
        $total += $_ for @values; 

        return $count ? $total / $count : 0;
}

sub stdev {
        my ($average, @values) = @_;

        my $count = scalar @values;
        my $stdev_sum = 0;
        $stdev_sum += ($_ - $average) ** 2 for @values;

        return $count ? sqrt($stdev_sum / $count) : 0;
}

while (<STDIN>) {
    chomp;
    my ($resptime) = /^\S+ \S+ \S+ \[.*?\] \".*?\" \S+ \S+ \".*?\" \".*?\" (\S+) .*/;
    foreach (@ARGV){
        if ($resptime > $_) {
            push @{ $items{$_} }, $resptime;
        }
    }
    push @{ $items{'all'} }, $resptime;
}

foreach (@ARGV , 'all') {
    $results{$_}{'n'} = scalar @{ $items{$_} } ? scalar @{ $items{$_} } : 0;
    $results{$_}{'avg'} = average( @{ $items{$_} });
    $results{$_}{'stdev'} = stdev($results{$_}{'avg'}, @{ $items{$_} });
}

print Dumper(\%results);

