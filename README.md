Usage
=====

scenario-a1.pl
--------------

scenario-a1.pl < *logfile*

scenario-a2.pl
--------------

tail -n 10000 *logfile* | scenario-a2.pl *threshold_1 threshold_2 ... threshold_n*

scenario-b.py
-------------

scenario-b.py < *list of urls*
